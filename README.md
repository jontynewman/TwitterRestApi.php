# TwitterRestApi.php
TwitterRestApi.php is a simple PHP class that is capable of accessing Twitter's REST API using either your application credentials or an access token.

## Getting Started
The example below will retrieve [this tweet] and display it as HTML.
```php
require 'TwitterRestApi.php';
$twitter = new TwitterRestApi(APP_KEY, APP_SECRET, TOKEN, TOKEN_SECRET, 'ca-bundle.crt');
$tweet = $twitter->oauth('GET', 'statuses/oembed', array('id' => '124939860069978112'));
echo $tweet['html'];
```
You will need to do the following before this example will work for you:
 - Replace `APP_KEY` with your application's consumer key.
 - Replace `APP_SECRET` with your application's consumer secret.
 - Replace `TOKEN` with your application's access token.
 - Replace `TOKEN_SECRET` with your application's access token secret.
 - Ensure that the SSL certificate file `'ca-bundle.crt'` is placed in one of the directories specified in your PHP [include_path] configuration option. If the file is in another directory, you can specify the absolute or relative path to the file instead (e.g. "path/to/ca-bundle.crt")

You can view your application credentials (such as the consumer key and secret) by [signing in] to the [Twitter Application Management] page.

## Creating Your Own Requests
This class is capable of making three types of authenticated requests to Twitter.

### Methods

 - **OAuth**

 ```php
 array oauth(string $method, string $endpoint [, array $query [, array $params]])
 ```

 Sends a request that makes use of your application credentials in order to authenticate. This is the most direct manner in which to access the API as it does not require obtaining an access token beforehand. 

 - **Bearer**

 ```php
 array bearer(string $method, string $endpoint, string $token [, array $query [, array $params]])
 ```

 Sends a request containing a valid access token in order to authenticate. You can obtain an access token using your application credentials by calling the `getAccessToken()` method (see below).

 - **Basic**

 ```php
 string getAccessToken()
 ```

 Retrieves an access token from Twitter using your application credentials. This method is useful when making a bearer request to the Twitter API (see above).


### Parameters
You may notice that some of these functions take similar parameters, each of which is explained below: 

 - **$method** - a string 

 The HTTP method of the request to be sent. This will either be 'GET' or 'POST', depending on the resource and the manner in which you want to access it. In order to determine the appropriate method, you should refer to the [Twitter REST API documentation]. 

 

 - **$endpoint** - a string 

 The location of the resource that you wish to access (e.g. `'search/tweets'`). A full list of accessible resources can be found in the [Twitter REST API documentation]. 

 

 - **$token** - a string 

 The access token to use in order to authenticate the request (note that this only applies to the `bearer()` method). You can obtain access tokens by calling `getAccessToken()`, which will return a string that can be used as the value of this parameter. 

 - **$query** *(optional)* - an associative array

 The query parameters to send as part of the request. Any parameters specified here will equate to the query string that is appended to the URL (i.e. all characters following '?' in `http://example.com/resource?param=value`). The type of this parameter should be an associative array, mapping keys to values accordingly (e.g. `array('screen_name' => 'TwitterAPI')`). For a list of parameters that can be specified when accessing a particular resource, refer to the [Twitter REST API documentation]. 

 - **$params** *(optional)* - an associative array 

 The parameters to send and part of the HTTP request. Any parameters specified here will equate to the query string that is to be used as the content of the HTTP request. The type of this parameter should be an associative array, mapping keys to values accordingly (e.g. `array('status' => 'Hello world!')`). For a list of parameters that can be specified when accessing a particular resource, refer to the [Twitter REST API documentation]. 

 

## Using the response  

Each of the methods mentioned above (with the exception of `getAccessToken()`) will return an associate array representing the response of the request (or `NULL` on failure). For example, running... 

```php
require 'TwitterRestApi.php';
$twitter = new TwitterRestApi(...);
$tweet = $twitter->oauth('GET', 'statuses/oembed', array('id' => '124939860069978112'));
print_r($tweet);
```

...will display the following text:

```
Array
(
    [cache_age] => 3153600000
    [url] => https://twitter.com/fakekurrik/statuses/124939860069978112
    [height] => 
    [provider_url] => https://twitter.com
    [provider_name] => Twitter
    [author_name] => fakekurrik
    [version] => 1.0
    [author_url] => https://twitter.com/fakekurrik
    [type] => rich
    [html] => <blockquote class="twitter-tweet"><p>Hello Ladies + Gentlemen, a signed OAuth request!</p>&mdash; fakekurrik (@fakekurrik) <a href="https://twitter.com/fakekurrik/statuses/124939860069978112">October 14, 2011</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    [width] => 550
)

```

This means that also running...

```php
echo " author  => " . $tweet['author_name'] . "\n";
echo "  width  => " . $tweet['width'] . "\n";
echo "version  => " . $tweet['version'] . "\n";
```

...will display:

```
 author  => fakekurrik
  width  => 550
version  => 1.0

```

## You're ready to go!


[this tweet]:https://twitter.com/fakekurrik/status/124939860069978112
[include_path]:http://www.php.net/manual/en/ini.core.php#ini.include-path
[signing in]:https://twitter.com/login?redirect_after_login=https%3A%2F%2Fapps.twitter.com%2F

[Twitter Application Management]:https://apps.twitter.com/
[Twitter REST API documentation]:https://dev.twitter.com/docs/api/1.1