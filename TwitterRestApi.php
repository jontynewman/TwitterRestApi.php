<?php

/**
 * A simple interface between the Twitter REST API and PHP.
 * 
 * This class is capable of performing three types of authentication: OAuth,
 * bearer and basic.
 * 
 * OAuth authentication makes use of your Twitter application credentials 
 * (consumer key, consumer secret, access token and access token secret). This
 * is the most direct manner in which to access the API as it does not require
 * obtaining an access token beforehand. It is possible to make use of this
 * type of authentication by calling the `oauth()' method.
 * 
 * Bearer authentication makes use of a supplied access token, which can be
 * obtained by sending a basic request to the Twitter API. It is possible to
 * make use of this type of authentication by calling the `bearer()' method and
 * supplying it with a valid access token.
 * 
 * Basic authentication is most commonly used by the Twitter API in order to
 * obtain access tokens. It is possible to obtain an access token in this manner
 * by calling the `getAccessToken()' method.
 * 
 * Each request to the API through this class is made up of 4 parts: the method,
 * the endpoint, the query (optional) and the parameters (optional).
 * 
 * The method is equivalent to the HTTP method of the request you are sending 
 * (i.e. `GET' or `POST').
 * 
 * The endpoint is equivalent to the Twitter resource that you wish to access
 * (e.g. `statuses/user_timeline').
 * 
 * The query is equivalent to the parameters specified in the query string that
 * is normally appended to the URL. This should be supplied to methods within
 * this class as an associative array
 * (e.g. array('screen_name' => 'TwitterAPI')).
 *
 * The parameters are equivalent to the parameters specified in the HTTP request 
 * that is being sent to the Twitter API. This should be supplied to methods 
 * within this class as an associative array
 * (e.g. array('status' => 'Hello world!')).
 * 
 * @author Jonty Newman <jonty.newman@gmail.com>
 * @version 1.0
 * @see https://dev.twitter.com/docs/api/1.1 Twitter REST API documentation
 */
class TwitterRestApi {
	
	const SCHEME = 'https';
	const DOMAIN = 'api.twitter.com';
	const VERSION = '1.1';
	
	private $key;
	private $secret;
	private $token;
	private $token_secret;
	private $certificate;
	
	/** 
	 * Creates an interface to the Twitter API using the credentials 
	 * specified.
	 * 
	 * @param string $key          the key of the application
	 * @param string $secret       the secret of the application
	 * @param string $token        the access token of the application
	 * @param string $token_secret the access token secret of the application
	 * @param string $certificate  file path to a valid SSL certificate
	 */
	public function __construct($key, $secret, $token, $token_secret, $certificate) {
		$this->key = $key;
		$this->secret = $secret;
		$this->token = $token;
		$this->token_secret = $token_secret;
		$this->certificate = $certificate;
	}
	
	/**
	 * Creates a complete URL to a Twitter REST API resource.
	 
	 * @param string $endpoint the resource to access (e.g. `search/tweets')
	 * @param string $format   (optional) the desired format of the response
	 *                         (e.g. `json')
	 * 
	 * @return string a complete URL containing a scheme, domain name, resource
	 *                endpoint and format (if any)
	 */
	public static function url($endpoint, $format = NULL) {
		$suffix = $format ? ".{$format}" : '';
		return self::SCHEME.'://'.self::DOMAIN.'/'.self::VERSION.'/'.$endpoint.$suffix;
	}
	
	/**
	 * Retrieves data from the Twitter REST API using an OAuth authenticated
	 * request.
	 * 
	 * @param string $method   the HTTP method of the request (`GET' or `POST')
	 * @param string $endpoint the resource to access (e.g. `search/tweets')
	 * @param array  $query    an associative array representing the query 
	 *                         parameters of the request
	 * @param array  $params   an associative array representing the HTTP
	 *                         parameters of the request
	 * 
	 * @return array|null the response of the request or NULL on failure
	 */
	public function oauth($method, $endpoint,
		array $query = NULL, array $params = NULL
	) {
		return json_decode(
			$this->sendOauthRequest(
				$method, $this->url($endpoint, 'json'),
				$query, $params
			),
			TRUE
		);
	}
	
	/**
	 * Retrieves data from the Twitter REST API using a bearer authenticated
	 * request.
	 * 
	 * @param string $method   the HTTP method of the request (`GET' or `POST')
	 * @param string $endpoint the resource to access (e.g. `search/tweets')
	 * @param string $token    the access token to use in order to authenticate
	 *                         the request
	 * @param array  $query    an associative array representing the query 
	 *                         parameters of the request
	 * @param array  $params   an associative array representing the HTTP
	 *                         parameters of the request
	 * 
	 * @return array|null the response of the request or NULL on failure
	 */
	public function bearer($method, $endpoint, $token,
		array $query = NULL, array $params = NULL
	) {
		return json_decode(
			$this->sendBearerRequest(
				$method, $this->url($endpoint, 'json'),
				$token, $query, $params
			),
			TRUE
		);
	}
	
	/**
	 * Retrieves an access token from Twitter using a basic authenticated
	 * request.
	 * 
	 * @return string|null a valid token with which to access the Twitter 
	 *                     REST API or NULL on failure
	 */
	public function getAccessToken() {
		$token = NULL;
		$resp = json_decode(
			$this->sendBasicRequest(
				'POST', self::SCHEME.'://'.self::DOMAIN.'/oauth2/token',
				'client_credentials'
			),
			TRUE
		);
		if ($resp && $resp['token_type'] == 'bearer') {
			$token = $resp['access_token'];
		}
		return $token;
	}
	
	private function sendOauthRequest($method, $url,
		array $query = NULL, array $params = NULL
	) {
		$oauth_params = array();
		$oauth_params['oauth_consumer_key'] = $this->key;
		$oauth_params['oauth_nonce'] = $this->generateOauthNonce(42);
		$oauth_params['oauth_signature_method'] = 'HMAC-SHA1';
		$oauth_params['oauth_timestamp'] = time();
		$oauth_params['oauth_token'] = $this->token;
		$oauth_params['oauth_version'] = '1.0';
		if (!$query) $query = array();
		if (!$params) $params = array();
		$signature = $this->generateOauthSignature($method, $url, 
			array_merge($oauth_params,$query,$params)
		);
		$oauth_params['oauth_signature'] = $signature;
		return $this->sendRequest($method, $url, $query,
			array("Authorization: OAuth {$this->formatOauthParams($oauth_params)}"),
			$params
		);
	}
	
	private function sendBearerRequest($method, $url, $token,
		array $query = NULL, array $params = NULL
	) {
		return $this->sendRequest($method, $url, $query,
			array("Authorization: Bearer {$token}"), $params
		);
	}
	
	private function sendBasicRequest($method, $url, $grant_type, 
		array $query = NULL
	) {
		$credentials = base64_encode(
			urlencode($this->key).':'.urlencode($this->secret)
		);
		return $this->sendRequest($method, $url, $query,
			array(
				"Authorization: Basic {$credentials}",
				'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
			),
			array('grant_type' => $grant_type)
		);
	}
	
	private function sendRequest($method, $url, array $query = NULL,
		array $headers = NULL, array $params = NULL
	) {
		$destination = $query ? $url.'?'.http_build_query($query) : $url;
		if (!$headers) $headers = array();
		if (!$params) $params = array();
		$options = array(
			'http' => array(
				'method' => strtoupper($method),
				'header' => implode("\r\n",$headers),
				'content' => http_build_query($params),
				'ignore_errors' => FALSE
			),
			'ssl' => array(
				'verify_peer' => TRUE,
				'cafile' => stream_resolve_include_path($this->certificate)
			)
		);
		$response = @file_get_contents(
			$destination,
			FALSE, 
			stream_context_create($options)
		);
		if ($response === FALSE) $response = NULL;
		return $response;
	}
	
	private function generateOauthNonce($length = 20) {
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$nonce = '';
		for ($i = 0; $i < $length; $i++) {
			$nonce .= $chars[rand(0, strlen($chars)-1)];
		}
		return $nonce;
	}
	
	private function generateOauthSignature($method, $url, array $params) {
		
		$signing_key = rawurlencode($this->secret).'&'.rawurlencode($this->token_secret);
		$base_str = strtoupper($method).'&'.rawurlencode($url).'&';
		$encoded_params = array();
		$param_strs = array();
		foreach ($params as $key => $value) {
			$encoded_params[rawurlencode($key)] = rawurlencode($value);
		}
		ksort($encoded_params);
		foreach ($encoded_params as $key => $value) {
			$param_strs[] = "{$key}={$value}"; 
		}
		$base_str .= rawurlencode(implode('&',$param_strs));
		return rawurlencode(base64_encode(
			hash_hmac('sha1',$base_str,$signing_key,TRUE)
		));
	}
	
	private function formatOauthParams(array $params) {
		$formatted_params = array();
		ksort($params);
		foreach ($params as $key => $value) {
			$formatted_params[] = "{$key}=\"{$value}\"";
		}
		return implode(', ',$formatted_params);
	}
}

?>